import React, {Component} from 'react';
import axios from 'axios';
import OrderItem from "../../components/Order/OrderItem/OrderItem";
import Spinner from "../../components/Spinner/Spinner";
import withErrorHandler from '../../hoc/withErrorHandler/withErrorHandler';

class Orders extends Component {
    state = {
        orders: [],
        loading: true
    };

    componentDidMount() {
        axios.get('/orders.json')
            .then(response => {
                const fetchedOrders = [];
                for (let key in response.data) {
                    fetchedOrders.push({...response.data[key], id: key});
                }
                this.setState({loading: false, orders: fetchedOrders});
            })
            .catch(() => {
                this.setState({loading: false});
            });
    }

    render() {
        let orders = this.state.orders.map(order => (
            <OrderItem key={order.id}
                       ingredients={order.ingredients}
                       price={order.price}
            />
        ));

        if (this.state.loading) {
            orders = <Spinner />;
        }

        return orders;
    }
}

export default withErrorHandler(Orders, axios);
